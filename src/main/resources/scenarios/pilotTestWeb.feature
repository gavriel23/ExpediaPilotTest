@ExpediaWebTest
Feature: Expedia flight

  @testWebExpedia
  Scenario: flight order
    Given I open browser to webpage "expedia.com"
    Then I am on home page of Expedia
    And I clean cookies of expedia site"
    And I click on "onlyFlight.button"
    And I click on "roundTrip.button"
    Then I enter "tel aviv" to "fromFlight"
    And I click on "select.Telaviv"
    And I enter "ewr" to "destination"
    And I click on "select.ewr"
    When I'm enter date of departing "07/25/2017" and date of returning "12/12/2017"
    And I click on "web.SearchButton"
    And I wait "30" seconds for "lowest.price" to appear
    Then I check that price small from "1500"
    And I click on "filter.web"
    And I wait for "3" seconds
    And I click on "sortest.duration"
    And I wait for "4" seconds
    Then I check that duration shoter than "18" hours


  @WebHotels2
  Scenario: hotel order
    Given I open browser to webpage "https://www.expedia.com"
    Then I delete cookies
    And I wait for "4" seconds
    Then I click on "hotel.button"
    And I clear destination field
    And I enter "Italy, Rome" to "fieldDestination.field"
    And I click on "destinationResults.button"
    And I enter my check-in date"7/13/2017"and check-out date"7/15/2017"
    And I click on "search.buttonWeb"
    And I choose a "4" star hotel
    And I wait for "7" seconds
    And I check if the hotel has free wifi and cost under "400" dollar

