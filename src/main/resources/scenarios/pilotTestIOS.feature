@expediaIos
Feature: Expedia Pilot Test

  @orderFlight
  Scenario: order flight
    Given I open application by name "Expedia"
    Then I search flight from "TLV" to "EWR"
    And  I click on "button.selectDates"
    Then  I choose flight "7" days from today
    And  I choose return flight "21" days from today
    Then I click on "button.selectDone"
    And I click on "button.search"
    And I sort by duration
    Then the flight duration is not more than "16" hours
    And I validate the flight price is not more than "1500" dollar
    Then I click on "button.selectFlight"
    And I sort by duration
    And I validate the flight price is not more than "1500" dollar
    Then I click on "button.selectFlight"


  @orderHotels
  Scenario: order hotel
    Given I open application by name "Expedia"
    Then I search hotel in "Rome"
    And I select hotel dates between "Friday, September 1" to "Sunday, September 3"
    Then I click on "button.selectTravelers"
    And I click on "button.select2Adult"
    And I click on "button.selectDone"
    Then I click on "button.search"
    Then I validate the hotel is rated "4" stars
    And validate free internet
    And I click on "button.selectFinal"
    Then I validate the total price is not more than "400"
