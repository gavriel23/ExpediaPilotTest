@expediaAndroid
Feature: Expedia Pilot Test

  @expedia1
  Scenario: order flight
    Given I go to the device home screen
    Given  I clean application by name "Expedia"
    Given I start application by name "Expedia"
    And validate if there is pop up
    And I search flight from "TLV" to "EWR"
    Then I choose flight "7" days from today
    And I choose return flight "21" days from today
    And I click on "button.Done"
    And I click on "button.Search"
    And I sort by duration
    Then the flight duration is not more than "16" hours
    Then validate the price is not more than "1500"
    And I try to click on "button.SelectFlightButton"
    And I sort by duration
    Then validate the price is not more than "1500"
    And I try to click on "button.SelectFlightButton"
    And I click on "button.CheckoutButton"

  @expedia2
  Scenario: order hotel
    Given I go to the device home screen
    Given  I clean application by name "Expedia"
    Given I start application by name "Expedia"
    And validate if there is pop up
    When I click on "button.Shop"
    And I click on "button.Hotels"
    And I wait "20" seconds for "form.Destination" to appear
    Then  I enter "Rome" to "form.Destination"
    And I wait "10" seconds for "menu.Rome" to appear
    And I click on "menu.Rome"
    And I select hotel dates between "Friday, September 1" to "Sunday, September 3"
    And I click on "button.Guests"
    And I click on "button.Plus"
    Then I click on "button.Done"
    And I click on "button.Search"
    Then I validate the hotel is rated "4" stars
    Then validate free internet
    And I wait "20" seconds for "button.SelectRoomButton" to appear
    Then I click on "button.SelectRoomButton"
    And I click on "button.HotelBookButton"
    And I validate the total price is not more than "400"




