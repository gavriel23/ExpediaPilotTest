package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static java.lang.Thread.sleep;


@QAFTestStepProvider
public class WebFlightSteps {
    private static QAFExtendedWebDriver getQAFdriver() {
        return new WebDriverTestBase().getDriver();
    }

    /*this function receive two dates and activate the function that calculate how many clicks on the next button
    @param departing is desired date of departing
    @param returning is desired date of returning
     */

    @When("^I'm enter date of departing \"(\\d{2}[ /-]\\d{2}[ /-]\\d{4})\" and date of returning \"(\\d{2}[ /-]\\d{2}[ /-]\\d{4})\"")
    public static void datesFlight(String departing, String returning) throws InterruptedException {
        QAFExtendedWebDriver driver = getQAFdriver();
        driver.findElement("departing.date").click();
        moveToDesireMonth(departing, driver.findElement("departing.date").getAttribute("value"));
        driver.findElement("returning.date").click();
        moveToDesireMonth(returning, driver.findElement("returning.date").getAttribute("value"));
    }

    @Given("^I am on home page of Expedia$")
    public static boolean homePageExpedia() {
        QAFExtendedWebDriver driver = getQAFdriver();
        return "https://www.expedia.com/".equals(driver.getCurrentUrl());

    }

    @Then("^I enter \"(.*?)\" to \"(.*?)\" and clear text field$")
    public static void iSet(String text, String locator) {
        CommonStep.clear(locator);
        CommonStep.sendKeys(text, locator);

    }

    @Then("^I check that price small from \"(.*)\"$")
    public static void checkCheapestPrice(int desirePrice) {
        QAFExtendedWebDriver driver = getQAFdriver();
        String priceLowest = driver.findElement("lowest.price").getText();
        System.out.println(Integer.valueOf(priceLowest.replaceAll("\\D", "")) < desirePrice);
    }

    @Then("^I check that duration shoter than \"(.*)\" hours$")
    public static void shrtestDurationInWeb(int desireDuration) {
        QAFExtendedWebDriver driver = getQAFdriver();
        String hoursDuration = driver.findElement("duration.text").getText().split("\\h")[0];
        Integer hoursint = Integer.valueOf(hoursDuration.substring(0, hoursDuration.length() - 1));
        System.out.println(hoursint < desireDuration);
    }

    /* this function calculate how many clicks on the next button
    @param textValue is  date to check
    @param textbox is date that appear in box date
     */
    private static void moveToDesireMonth(String textValue, String textbox) {
        QAFExtendedWebDriver driver = getQAFdriver();
        Integer numMonth = Integer.valueOf(textValue.substring(0, 2));
        Integer numYear = Integer.valueOf(textValue.substring(6));
        Integer dayOfMonthDeparting = Integer.valueOf(textValue.substring(3, 5));
        if (textbox.isEmpty()) {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Calendar thisDate = Calendar.getInstance();
            textbox = dateFormat.format(thisDate.getTime());
        }
        Integer Monthinbox = Integer.valueOf(textbox.substring(0, 2));
        Integer Yearinbox = Integer.valueOf(textbox.substring(6));
        for (int i = 0; i < Math.abs((numYear * 12 + numMonth) - (Yearinbox * 12 + Monthinbox)); i++) {
            if ((numYear * 12 + numMonth) > (Yearinbox * 12 + Monthinbox))
                click("next.button");
            else if ((numYear * 12 + numMonth) < (Yearinbox * 12 + Monthinbox))
                click("prev.button");
        }
        driver.findElement("//*[contains(@class,\"datepicker-cal-date\") and @data-month="
                + (numMonth - 1) + " and text()=" + dayOfMonthDeparting + "]").click();
    }

    @Then("^I clean cookies of expedia site\"$")
    public static void cleanPage() throws InterruptedException {
        System.out.println(getQAFdriver().getCurrentUrl());
        if (homePageExpedia()) {
            sleep(20000);
        }
        getQAFdriver().manage().deleteAllCookies();
        getQAFdriver().navigate().refresh();
        getQAFdriver().manage().timeouts().pageLoadTimeout(4, TimeUnit.SECONDS);
    }

    @And("^I'm enter date of checkin \"(\\d{2}[ /-]\\d{2}[ /-]\\d{4})\" and date of checkout \"(\\d{2}[ /-]\\d{2}[ /-]\\d{4})\"")
    public static void enterDatesOfCheckinAndCheckout(String checkin, String checkout) {
        getQAFdriver().findElement("date.checkin").click();
        moveToDesireMonth(checkin, getQAFdriver().findElement("date.checkin").getAttribute("value"));
        getQAFdriver().findElement("date.checkout").click();
        moveToDesireMonth(checkout, getQAFdriver().findElement("date.checkout").getAttribute("value"));
    }

    private static void changeOptions(String loc, String askingOption) {
        Select pricelow = new Select(getQAFdriver().findElement(loc));
        pricelow.selectByVisibleText(askingOption);
    }

    @Then("^I am filter result of hotels by price$")
    public static void filterPrice() {
        if (!"Sort & Filter".equals(getQAFdriver().findElement("filter.button").getText())) {
            changeOptions("recommend.button", "Price");
        }
    }

    @Then("^I am choose result of hotels with \"(\\d)\" star$")
    public static boolean chooseStar(String numStar) {
        if ("Sort & Filter".equals(getQAFdriver().findElement("filter.button").getText())) {
            click("filterSort");
            click("//*[@class=\"property-class-input\"]/span[" + numStar + "]");
            changeOptions("Recommend.button", "Price");
            click("done.button");
            return false;
        }
        click("filter.button");
        click("//*[@id=\"star" + numStar + '"' + "]");
        click("Done.button");
        return true;
    }

    @And("^I check that cheapest price small than \"(.*)\" dollar$")
    public static boolean cheapestHotel(int askingPrice) {
        for (int i = 1; i < 6; i++) {
            getQAFdriver().findElement(By.xpath("(//*[@class='flex-link'])[" + i + "]")).click();
            QAFWebElement priceOfRoom = getQAFdriver().findElement("hotelRoomElement.element");
            String alltextinBox = priceOfRoom.getText();
            if (alltextinBox.contains("Free Internet")) {
                Double price = Double.valueOf(priceOfRoom.findElement("totalPrice.value").getAttribute("value"));
                return price < askingPrice;
            }
            DeviceUtils.pressKey("BACK");
        }
        return false;
    }
}
