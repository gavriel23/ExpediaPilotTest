package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.waitForPresent;
import static com.quantum.utils.DeviceUtils.getQAFDriver;
import static java.lang.Thread.sleep;


@QAFTestStepProvider
public class WebHotelSteps {

    @Then("I delete cookies")
    public static void deleteCookies() throws InterruptedException {
        if (!("https://www.expedia.com/").equals(getQAFDriver().getCurrentUrl()))
            sleep(2000);
        getQAFDriver().manage().deleteAllCookies();
        getQAFDriver().navigate().refresh();
    }

    @When("I clear destination field")
    public static void clearField() {
        CommonStep.clear("fieldDestination.field");
        getQAFDriver().manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
    }

    @When("^I enter my check-in date\"(\\d{1,2}\\/\\d{1,2}\\/\\d{4})" +
            "\"and check-out date\"(\\d{1,2}\\/\\d{1,2}\\/\\d{4})\"$")
    public void occupancyDates(String checkInDate, String checkOutDate) {
        try {
            click("//*[@id=\"hotel-checkin-hp-hotel\"]");
            Date checkInDateObject = new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(checkInDate);
            String correctCheckInMonth = new SimpleDateFormat("MMM YYYY", Locale.US).format(checkInDateObject);
            String startDayOfMonth = new SimpleDateFormat("d", Locale.US).format(checkInDateObject);
            String startMonth = new SimpleDateFormat("M", Locale.US).format(checkInDateObject);
            int domMonthStartDate = Integer.parseInt(startMonth) - 1;

            Date checkOutDateObject = new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(checkOutDate);
            String correctCheckOutMonth = new SimpleDateFormat("MMM YYYY", Locale.US).format(checkOutDateObject);
            String endDayOfMonth = new SimpleDateFormat("d", Locale.US).format(checkOutDateObject);
            String endMonth = new SimpleDateFormat("M", Locale.US).format(checkOutDateObject);
            int domMonthEndDate = Integer.parseInt(endMonth) - 1;
            while (!verifyMonth(correctCheckInMonth)) {
                click("nextMonth.button");
            }
            click("//button[@class=\"datepicker-cal-date\" and @data-month=" + domMonthStartDate + " and text()=" + startDayOfMonth + "]");
            click("//*[@id=\"hotel-checkout-hp-hotel\"]");
            while (!verifyMonth(correctCheckOutMonth)) {
                click("nextMonth.button");
            }
            click("//button[@class=\"datepicker-cal-date\" and @data-month=" + domMonthEndDate + " and text()=" + endDayOfMonth + "]");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static boolean verifyMonth(String desiredMonth) {
        QAFExtendedWebElement month = new QAFExtendedWebElement("monthHeader.text");
        String monthHeader = month.getText();
        return monthHeader.equals(desiredMonth);
    }

    private static void changeOptions(String loc, String askingOption) {
        Select byPrice = new Select(getQAFDriver().findElement(loc));
        byPrice.selectByVisibleText(askingOption);
    }


    @Then("^I choose a \"(\\d*)\" star hotel$")
    public static void chooseStar(String numStar) {
        if ("SORT & FILTER".equalsIgnoreCase(getQAFDriver().findElement("filter.button").getText())) {
            click("filterSort");
            click("//*[@class=\"property-class-input\"]/span[" + numStar + "]/label[1]");
            changeOptions("Recommended.button", "Price");
            click("doneWeb2.button");
        }
        click("filter.button");
        click("//*[@id=\"star" + numStar + '"' + "]");
        click("doneWeb.button");
        changeOptions("recommended.button", "Price");
    }

    @When("^I check if the hotel has free wifi and cost under \"(\\d*)\" dollar$")
    public static boolean freeWifi(double maxPrice) throws InterruptedException {
        int instanceOf = 1;
        while (true) {
            QAFWebElement hotelElement = getQAFDriver().findElement(By.xpath("(//*[@class='flex-link'])" + "[" + instanceOf + "]"));
            hotelElement.click();
            waitForPresent("hotelRoomElement.element");
            QAFWebElement roomElement = getQAFDriver().findElement("hotelRoomElement.element");
            String internet = roomElement.getText();
            if (internet.contains("Free Internet")) {
                String totalPrice = getQAFDriver().findElement("totalPrice.value").getAttribute("value");
                if (Double.parseDouble(totalPrice) < maxPrice)
                    System.out.printf("A room is available under %s for %s", String.valueOf(maxPrice), totalPrice);
                return true;
            }
            DeviceUtils.pressKey("BACK");
            instanceOf++;
        }
    }

//    private static boolean isPresent(String loc, int second) {
//        try {
//            CommonStep.waitForPresent(loc, second);
//            return true;
//        } catch (TimeoutException te) {
//            return false;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    private static boolean isPresent(String loc) {
//        try {
//            CommonStep.waitForPresent(loc);
//            System.out.println(loc + " is present on the page");
//            return true;
//        } catch (TimeoutException te) {
//            System.out.println(loc + " is not present on the page");
//            return false;
//        } catch (Exception e) {
//            System.out.println("something wrong!!, check your connection");
//            return false;
//        }
//    }
//
//    private static void clickIfPresent(String loc, int second) {
//        if (isPresent(loc, second)) {
//            getQAFDriver().findElement(loc).click();
//        }
//    }
}
