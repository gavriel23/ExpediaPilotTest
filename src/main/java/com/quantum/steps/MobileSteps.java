package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.Utils.AndroidUtils;
import com.quantum.Utils.ExpediaUtils;
import com.quantum.Utils.IosUtils;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.TimeoutException;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.quantum.steps.PerfectoApplicationSteps.iSet;
import static com.quantum.utils.DeviceUtils.switchToContext;

@QAFTestStepProvider
public class MobileSteps {

    private ExpediaUtils expediaUtils;
    private String platformName;
    private AndroidUtils androidUtils = new AndroidUtils();

    public MobileSteps() {
        platformName = new WebDriverTestBase().
                getDriver().getCapabilities().getCapability("platformName").toString();
        if (platformName.equals("android"))
            expediaUtils = new AndroidUtils();
        else if (platformName.equals("iOS"))
            expediaUtils = new IosUtils();
    }

    @Then("^I choose flight \"(.+)\" days from today")
    public void chooseFlight(int days) throws Throwable {
        expediaUtils.chooseDate(days);
    }

    @And("^I choose return flight \"(.+)\" days from today")
    public void chooseReturnFlight(int days) throws Throwable {
        expediaUtils.chooseDate(days);
    }

    @Then("I validate the flight price is not more than \"(.+)\" dollar")
    public void validateFlightPrice(String price) throws Throwable {
        expediaUtils.validateFlightPrice(price);
    }

    @Then("the flight duration is not more than \"(.+)\" hours")
    public void validateFlightDuration(int time) throws Throwable {
//        expediaUtils.validateFlightDuration(time);
        expediaUtils.saveDuration(time);
//        System.out.println(expediaUtils.setDuration(time));
    }

    @Then("validate free internet")
    public void validateHotelOrder() throws Throwable {
        expediaUtils.validateFreeInternet();
    }

    @Then("I validate the total price is not more than \"(.+)\"")
    public void validateTotal(String limitPrice) throws Throwable {
        expediaUtils.validateTotalPrice(limitPrice);
    }

    @When("^I search hotel in \"([^\"]*)\"$")
    public void hotelsSearch(String destination) throws Throwable {
        expediaUtils.safeClick("button.hotels", 15);
        clear("button.locationSuggestion");
        iSet(destination, "button.locationSuggestion");
        click("button.selectRome");
    }

    @And("validate if there is pop up")
    public void validatePopUps() throws Throwable {
        if (androidUtils.checkPopup("alert.img", 10)) {
            click("alert.img");
            click("button.LetsGo");
        }
        androidUtils.checkPopup("alert.Allow", 10);
        androidUtils.checkPopup("alert.SignIn", 10);
    }

    @And("I search flight from \"(.+)\" to \"(.+)\"")
    public void searchFlight(String from, String to) throws Throwable {
        expediaUtils.searchFlight(from, to);
    }

    @And("I select hotel dates between \"(.+)\" to \"(.+)\"")
    public void selectDatesHotel(String fromDay, String untilDay) throws Throwable {
        expediaUtils.chooseDate(fromDay);
        expediaUtils.chooseDate(untilDay);
        click("button." + (platformName.equals("android") ? "Done" : "selectDone"));
    }

    @Then("^I open application by name \"(.*?)\"")
    public void StartApp(String name) {
            DeviceUtils.closeApp(name, "name");
            DeviceUtils.startApp(name, "name");
            switchToContext("NATIVE_APP");
    }

    @And("I sort by duration")
    public void sortByDuration() throws Throwable {
        expediaUtils.sortByDuration();
    }

    @Then("I validate the hotel is rated \"(.+)\" stars")
    public void validateStars(int rating) throws Throwable {
        expediaUtils.chooseRating(rating);
    }

    @Then("validate the price is not more than \"(.+)\"")
    public void validatePrice(String price) throws Throwable {
        expediaUtils.validateFlightPrice(price);
    }

    @And("I try to click on \"(.+)\"")
    public void tryToClick(String clickAble) throws Throwable {
        try {
            click(clickAble);
        } catch (TimeoutException e) {
            System.out.println("Button not found: " + clickAble);
        }
    }
}
