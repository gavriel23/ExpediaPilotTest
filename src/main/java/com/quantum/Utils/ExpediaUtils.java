package com.quantum.Utils;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.util.LocatorUtil;
import com.quantum.steps.PerfectoApplicationSteps;
import cucumber.deps.com.thoughtworks.xstream.mapper.Mapper;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static com.qmetry.qaf.automation.step.CommonStep.click;

/**
 * Created by yonir on 7/5/2017.
 */
public abstract class ExpediaUtils {

    String patternForChooseDate = null;
    String nextMonthButton = null;

    private QAFExtendedWebDriver driver = new WebDriverTestBase().getDriver();

    private String getFutureDate(int days) {
        DateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM d");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_WEEK, days);
        Date currentDatePlusOne = c.getTime();
        return dateFormat.format(currentDatePlusOne);
    }

    public void safeClick(String loc, int timeOut) {

        PerfectoApplicationSteps.switchToContext("NATIVE_APP");
        FluentWait<WebDriver> await = new FluentWait<WebDriver>(getDriver())
                .withTimeout(timeOut, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        await.until(ExpectedConditions.elementToBeClickable(LocatorUtil.getBy(loc))).click();
    }

    public void saveDuration(int time) {
        Properties prop = new Properties();
        OutputStream output;
        try {
            output = new FileOutputStream("config.properties");
            prop.setProperty("duration", String.valueOf(time));
            prop.store(output, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public String getDuration() {
        Properties prop = new Properties();
        InputStream input;
        String time = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            time = prop.getProperty("duration");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return time;
    }

    public QAFExtendedWebDriver getDriver() {
        return driver;
    }

    public void chooseDate(int days) {
        chooseDate(getFutureDate(days));
    }

    public void chooseDate(String date) {
        String pattern = patternForChooseDate + date + "\"]";
        while (true) {
            try {
                safeClick(pattern, 10);
                return;
            } catch (TimeoutException e) {
                click(nextMonthButton);
            }
        }
    }

    public abstract void validateFlightPrice(String price) throws Exception;

    public abstract void validateFreeInternet();

    public abstract void validateTotalPrice(String testDataPrice) throws Exception;

    public abstract void installApp(String appName);

    public abstract void chooseRating(int rating);

    public abstract void searchFlight(String from, String to);

    public abstract void sortByDuration();

}
