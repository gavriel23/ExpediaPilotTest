package com.quantum.Utils;

import com.quantum.utils.DeviceUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.qmetry.qaf.automation.step.CommonStep.clear;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.quantum.steps.PerfectoApplicationSteps.iSet;


public class IosUtils extends ExpediaUtils {

    public IosUtils() {
        super();
        patternForChooseDate = "//*[@label=\"";
        nextMonthButton = "button.NextMonthIos";
    }

    @Override
    public void validateFlightPrice(String priceLimit) throws Exception {
        for (int cnt = 3; ; cnt += 3) {
            for (int i = 0; i < cnt; i++) {
                verifyHours(i);
                if (isPriceOk(priceLimit, i))
                    return;
            }
            DeviceUtils.swipe("50%,60%", "50%,20%");
        }
    }

    private boolean isPriceOk(String priceLimit, int i) {
        boolean priceOk = false;
        WebElement ele = getDriver().findElementById("flight_search_result_cell_" + i + "deltaPrice");
        Integer flightPrice = getRegexNumber(ele.getText().
                replace("$", "").replace(",", ""));
        if (flightPrice <= Integer.parseInt(priceLimit)) {
            ele.click();
            priceOk = true;
        }
        return priceOk;
    }

    private void verifyHours(int i) throws Exception {
        WebElement hoursEle = getDriver().findElementById(
                "flight_search_result_cell_" + i + "durationAndStops");
        String hours = hoursEle.getText().split(" ")[0];
        Integer flightHours = getRegexNumber(hours);
        if (flightHours > Integer.parseInt(getDuration())) {
            throw new Exception("The requested flight could not be found");
        }
    }

    @Override
    public void validateFreeInternet() {
        Integer listCnt = 1;
        Integer cnt = 0;
        while (true) {
            cnt += 3;
            for (; listCnt < cnt; listCnt++) {
                if (listCnt >= getResultsNum()) {
                    return;
                }
                WebElement ele = getDriver().findElementById(String.format("priceLabel_%s", listCnt));
                ele.click();
                if (isFreeInternet())
                    return;
            }
            DeviceUtils.swipe("50%,60%", "50%,20%");
        }
    }

    private Integer getResultsNum() {
        try {
            return getRegexNumber(getDriver().findElement(By.id("hotel-table-header-title")).getText());
        } catch (Exception e) {
            return 10;
        }
    }

    private boolean isFreeInternet() {
        try {
            WebDriverWait wait = new WebDriverWait(getDriver(), 10);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(
                    (By.xpath("//*[@label=\"Free Internet amenity\"]"))));
            safeClick("button.selectRoom", 10);
            return true;
        } catch (NoSuchElementException e) {
            safeClick("button.back", 10);
        }
        return false;
    }

    @Override
    public void validateTotalPrice(String limitPrice) throws Exception {
        safeClick("button.selectNext", 15);
        Double sumResults = Double.valueOf(getDriver().findElement(By.id("total_amount_label")).getText()
                .split(". ")[2].replace("$", ""));
        checkPrice(Integer.parseInt(limitPrice), sumResults);
    }

    private Integer getRegexNumber(String results) {
        Pattern intsOnly = Pattern.compile("\\d+");
        Matcher makeMatch = intsOnly.matcher(results);
        //noinspection ResultOfMethodCallIgnored
        makeMatch.find();
        String inputInt = makeMatch.group();
        return Integer.valueOf(inputInt);
    }

    public void installApp(String name) {
        try {
            DeviceUtils.installApp("PRIVATE:applications/Expedia.ipa", false);
            DeviceUtils.startApp(name, "name");
        } catch (Exception e) {
            System.out.println("The app is not installed on ios device" +
                    " please install manually and run again");
        }
    }

    @Override
    public void chooseRating(int rating) {
//        try {
//            DeviceUtils.swipe("40%,60%", "30%,20%");
//            clickOnSort();
            Map<String, Object> pars = new HashMap<>();
            getDriver().executeScript("mobile:objects.optimization:start", pars);
            safeClick("button.selectSort&Filter", 35);
            getDriver().executeScript("mobile:objects.optimization:stop", pars);
            safeClick("button.sortBy", 10);
            safeClick("button.sortByPrice", 10);
            click("button." + rating + "Stars");
            safeClick("button.selectDone", 10);
//        } catch (Exception e) {
//            chooseOtherFilter(rating);
//        }
    }

    private void chooseOtherFilter(int rating) {
        try {
//            DeviceUtils.swipe("40%,60%", "30%,20%");
            safeClick("button.selectMap", 25);
            safeClick("button.selectFilter", 15);
            safeClick("button." + String.valueOf(rating) + "stars", 10);
            click("button.Price1");
            click("button.Price2");
            click("button.selectDone");
            click("button.selectList");
        } catch (Exception e) {
            System.out.println("the filter fail");
        }
    }

    private void checkPrice(int limitPrice, Double sumResults) throws Exception {
        if (sumResults > (limitPrice)) {
            throw new Exception("No results found for less than" + String.valueOf(limitPrice) + "$");
        } else {
            System.out.println("The test passed");
        }
    }

    @Override
    public void searchFlight(String from, String to) {
        safeClick("button.flight", 20);
        click("button.flyingFrom");
        clear("button.fromLoc");
        iSet(from, "button.fromLoc");
        safeClick("button.selectChoice", 10);
//        safeClick("button.fromLoc", 25);
//        clear("button.fromLoc");
        iSet(to, "button.fromLoc");
        safeClick("button.selectTo", 10);
    }

    @Override
    public void sortByDuration() {
        safeClick("button.sort", 25);
        click("button.shortestDuration");
        click("button.selectDone");
    }
}
