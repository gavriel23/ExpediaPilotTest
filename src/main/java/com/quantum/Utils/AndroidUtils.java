package com.quantum.Utils;

import com.quantum.utils.DeviceUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.qmetry.qaf.automation.step.CommonStep.clear;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;


/**
 * Created by yonir on 7/5/2017.
 */
public class AndroidUtils extends ExpediaUtils {

    public AndroidUtils() {
        super();
        patternForChooseDate = "//*[@content-desc=\"";
        nextMonthButton = "button.NextMonthAndroid";
    }

    public boolean checkPopup(String loc, int timeOut) {

        try {
            safeClick(loc, timeOut);
            System.out.println("Press the button to get out of pop-up");
            return true;
        } catch (TimeoutException t) {
            System.out.println("no pop-up showed up");
            return false;
        }
    }

    @Override
    public void validateFlightPrice(String testDataPrice) throws Exception {
        while (true) {
            List<WebElement> prices = getDriver().findElementsById("com.expedia.bookings:id/price_text_view");
            for (int i = 0; i < prices.size(); i++) {
                Double doublePrice = getDoublePrice(prices.get(i).getText());
                boolean isOkDuratin = checkDurationDuring(i);
                if (doublePrice <= Double.parseDouble(testDataPrice) && isOkDuratin) {
                    if (i == prices.size() - 1)
                        DeviceUtils.swipe("50%,60%", "50%,20%");
                    prices.get(i).click();
                    return;
                }
            }
            DeviceUtils.swipe("50%,60%", "50%,20%");
        }
    }

    private boolean checkDurationDuring(int index) throws Exception {
        List<WebElement> duration = getDriver().findElementsById(
                "com.expedia.bookings:id/flight_duration_text_view");
        try {
            if (getDoubTime(duration.get(duration.size()-1).getText()) > Double.parseDouble(getDuration()))
                throw new Exception("The duration is more than: " + getDuration());
            Double time = getDoubTime(duration.get(index).getText());
            if (time <= Double.parseDouble(getDuration()))
                return true;
        } catch (IndexOutOfBoundsException e) {
            return true;

        }
        return false;
    }

    @Override
    public void validateFreeInternet() {
        while (true) {
            List<WebElement> prices = getDriver().findElementsById("com.expedia.bookings:id/price_per_night");
            boolean isSponsored = isSponsored();
            for (int i = 0; i < prices.size(); i++) {
                if (isSponsored && i == 0) {
                    DeviceUtils.swipe("50%,60%", "60%,40%");
                    continue;
                }
                prices.get(i).click();
                try {
                    getDriver().findElement("text.Internet");
                    return;
                } catch (NoSuchElementException e) {
                    click("button.Back");
                }
                DeviceUtils.swipe("50%,60%", "50%,60%");
            }
        }
    }

    private boolean isSponsored() {
        try {
            return getDriver().findElementById(
                    "com.expedia.bookings:id/top_amenity").isDisplayed();
        } catch (NoSuchElementException e) {
            System.out.println("no sponsored ad");
            return false;
        }
    }

    @Override
    public void validateTotalPrice(String testDataPrice) throws Exception {

        WebElement price = getDriver().findElement("button.TotalPriceWithTax");
        Double doublePrice = getDoublePrice(price.getText());
        if (doublePrice > Double.parseDouble(testDataPrice))
            throw new Exception("The price is more than: " + testDataPrice);
    }

    public void installApp(String name) {
        DeviceUtils.installApp("PRIVATE:applications/com.expedia.bookings-8.21.0@APK4Fun.com.apk",
                true);
    }

    private double getDoubTime(String time) {
        String[] strTime = time.split(" ");
        String clock = strTime[0] + strTime[1];
        clock = clock.replace("h", ".").replace("m", "");
        return Double.parseDouble(clock);
    }

    @Override
    public void chooseRating(int stars) {
        safeClick("button.FilterText", 30);
        click("menu.SortBy");
        click("menu.Price");
        click("button." + stars + "Star");
        click("button.Search");
    }


    private Double getDoublePrice(String price) {
        String strPrice = price.replace("$", "").replace(",", "");
        return Double.parseDouble(strPrice);
    }

    @Override
    public void searchFlight(String from, String to) {
        click("button.Shop");
        click("button.Flights");
        safeClick("form.From", 10);
        clear("form.Destination");
        sendKeys(from, "form.Destination");
        try {
            safeClick("menu.AllAirports", 20);
        }
        catch (Exception e){
            sendKeys(from, "form.Destination");
            safeClick("menu.AllAirports", 20);
        }
        sendKeys(to, "form.Destination");
        click("menu.Newark");
    }

    @Override
    public void sortByDuration() {
        safeClick("button.SortFilter", 35);
        click("menu.Price");
        click("menu.Duration");
        safeClick("button.Search", 5);
    }
}
