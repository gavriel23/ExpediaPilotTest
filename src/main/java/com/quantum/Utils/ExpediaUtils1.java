package com.quantum.Utils;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.LocatorUtil;
import com.quantum.steps.PerfectoApplicationSteps;
import com.quantum.utils.DeviceUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.qmetry.qaf.automation.step.CommonStep.click;

@QAFTestStepProvider
public class ExpediaUtils1 {
    public static boolean checkPopup(QAFExtendedWebDriver driver, int timeout, QAFExtendedWebElement xpathObj) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PerfectoApplicationSteps.switchToContext("NATIVE_APP");
        FluentWait<WebDriver> await = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        try {
            await.until(ExpectedConditions.elementToBeClickable(xpathObj));
            xpathObj.click();
            System.out.println("Press the button to get out of pop-up");
            return true;
        } catch (Exception t) {
            System.out.println("no pop-up showed up");
            return false;
        }
    }

    private static String getFutureDate(int days) {
        DateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM d");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_WEEK, days);
        Date currentDatePlusOne = c.getTime();
        return dateFormat.format(currentDatePlusOne);
    }

    public static void chooseDate(QAFExtendedWebDriver driver, int days) {
        String platformName = driver.getCapabilities().getCapability("platformName").toString();
        String pattern;
        boolean isExist;
        if (platformName.equals("android"))
            pattern = "//*[@content-desc=\"" + getFutureDate(days) + "\"]";
        else
            pattern = "//*[@label=\"" + getFutureDate(days) + "\"]";
        do {
            try {
                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(pattern))).click();
                isExist = false;
            } catch (TimeoutException e) {
                if (platformName.equals("android")) {
                    click("button.NextMonthAndroid");
                } else {
                    click("button.NextMonthIos");
                }
                isExist = true;
            }
        } while (isExist);
    }

    public static void chooseDate(QAFExtendedWebDriver driver, String date) {
        String platformName = driver.getCapabilities().getCapability("platformName").toString();
        String pattern;
        boolean isExist;
        if (platformName.equals("android"))
            pattern = "//*[@content-desc=\"" + date + "\"]";
        else
            pattern = "//*[@label=\"" + date + "\"]";
        do {
            try {
                WebDriverWait wait = new WebDriverWait(driver, 2);
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(pattern))).click();
                isExist = false;
            } catch (TimeoutException e) {
                if (platformName.equals("android")) {
                    click("button.NextMonthAndroid");
                } else {
                    click("button.NextMonthIos");
                }
                isExist = true;
            }
        } while (isExist);
    }

    public static void ValidateDuration(QAFExtendedWebDriver driver, Map<String, String> testData) {

        boolean isShort = true;
        while (isShort) {
            List<WebElement> duration = driver.findElementsById("com.expedia.bookings:id/flight_duration_text_view");
            for (WebElement time : duration) {
                String[] strTime = time.getText().split(" ");
                String clock = strTime[0] + strTime[1];
                clock = clock.replace("h", ".").replace("m", "");
                double doubTime = Double.parseDouble(clock);
                if (doubTime <= 16.00) {
                    System.out.println("click on this: " + clock);
                    time.click();
                    isShort = false;
                    break;
                } else
                    System.out.println("not click on :" + clock);
                DeviceUtils.swipe("50%,60%", "50%,20%");
            }
        }
    }


    public static void validatePrice(QAFExtendedWebDriver driver, String testDataPrice, String id) {
        boolean isMore = true;
        while (isMore) {
            List<WebElement> prices = driver.findElementsById(id);
            for (WebElement price : prices) {
                Double intPrice = getDoublePrice(price);
                if (intPrice <= Double.parseDouble(testDataPrice)) {
                    price.click();
                    isMore = false;
                    break;
                } else {
                    DeviceUtils.swipe("50%,60%", "50%,20%");
                }
            }
        }
    }

    public static Double getDoublePrice(WebElement price) {
        String strPrice = price.getText().replace("$", "").replace(",", "");
        return Double.parseDouble(strPrice);
    }

    public static void validateInternetAndPrice(QAFExtendedWebDriver driver, String id) {
        boolean isMore = true;
        while (isMore) {
            List<WebElement> prices = driver.findElementsById(id);
            boolean isSponsored = false;
            try {
                isSponsored = driver.findElementById(
                        "com.expedia.bookings:id/top_amenity").isDisplayed();
            } catch (NoSuchElementException e) {
                System.out.println("no sponsored ed");
            }
            for (int i = 0; i < prices.size(); i++) {
                if (isSponsored && i == 0) {
                    DeviceUtils.swipe("50%,60%", "60%,40%");
                    continue;
                }
                prices.get(i).click();
                try {
                    driver.findElement("text.Internet");
                    isMore = false;
                    break;
                } catch (NoSuchElementException e) {
                    click("button.Back");
                }
                DeviceUtils.swipe("50%,60%", "50%,60%");
            }
        }
    }

    public static void ValidateFlightPrice(QAFExtendedWebDriver driver, String priceLimit, String HoursLimit) throws Exception {
        Thread.sleep(3000);
        Integer cnt = 3;
        while (true) {
            for (int i = 0; i < cnt; i++) {
                WebElement hoursEle = driver.findElementById(String.format
                        ("flight_search_result_cell_%sdurationAndStops", i));
                String hours = hoursEle.getText().split(" ")[0];
                Integer flightHours = getRejexNumber(hours);
                if (flightHours > Integer.parseInt(HoursLimit)) {
                    throw new Exception("The requested flight could not be found");
                }
                WebElement ele = driver.findElementById(String.format("flight_search_result_cell_%sdeltaPrice", i));
                Integer flightprice = getRejexNumber(ele.getText().
                        replace("$", "").replace(",", ""));
                if (flightprice <= Integer.parseInt(priceLimit)) {
                    ele.click();
                    return;
                }
            }
            DeviceUtils.swipe("50%,60%", "50%,20%");
            cnt += 3;
        }

    }


    public static void validate4Stars() throws InterruptedException {
        try {
            Thread.sleep(2000);
            DeviceUtils.swipe("50%,60%", "50%,20%");
            DeviceUtils.swipe("40%,60%", "30%,20%");
            Thread.sleep(3000);
            click("sortAndFilterButton");
            click("button.fourStar");
            click("button.sortBy");
            click("button.sortByPrice");
            click("button.selectDone");
        } catch (Exception e) {
            try {
                click("button.selectMap");
                click("button.selectFilter");
                click("button.fourStar");
                click("button.Price1");
                click("button.Price2");
                click("button.selectDone");
                click("button.selectList");
            } catch (Exception e1) {
                System.out.println("The filter entry failed");
            }
        }
    }

    public static void ValidateHotelPrice(QAFExtendedWebDriver driver) {
        Integer cnt = 3;
        Integer numResults = getRejexNumber(driver.findElement(By.id("hotel-table-header-title")).getText());
        while (true) {
            for (int i = 1; i < cnt; i++) {
                if (i >= numResults) {
                    return;
                }
                WebElement ele = driver.findElementById(String.format("priceLabel_%s", i));
                ele.click();
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 10);
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(
                            (By.xpath("//*[@label=\"Free Internet amenity\"]"))));
                    click("button.selectRoom");
                    return;
                } catch (NoSuchElementException e) {
                    click("button.back");
                }
            }
            DeviceUtils.swipe("50%,60%", "50%,20%");
            cnt += 3;
        }
    }

    public static void ValidateTotalPrice(QAFExtendedWebDriver driver, int limitPrice) throws Exception {
        driver.findElement("button.selectNext").click();
        Double sumResults = Double.valueOf(driver.findElement(By.id("total_amount_label")).getText()
                .replace("$", ""));
        if (sumResults > (limitPrice)) {
            throw new Exception("No results found for less than  400 $");
        } else {
            System.out.println("The test passed");
        }
    }

    private static Integer getRejexNumber(String results) {
        Pattern intsOnly = Pattern.compile("\\d+");
        Matcher makeMatch = intsOnly.matcher(results);
        makeMatch.find();
        String inputInt = makeMatch.group();
        return Integer.valueOf(inputInt);
    }

}