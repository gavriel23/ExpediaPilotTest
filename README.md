``          **Expedia - Pilot Test**

Sumery: 
* Test sanity on browser, Android and Mac

BBD Testing: The tests are written in clear language and easy to understand 
even for those who do not develop

**_The project's description:_**
Testing an Expedia, app and Website, 
By booking a flight / hotel in accordance with time, price restrictions and location.

**_Prerequisites:_**
Java 8
An IDE to write your tests on - Eclipse or IntelliJ
Maven

_Eclipse users should also install:_
Maven Plugin
TestNG Plugin
QAF Cucumber Plugin. Or go to  install new software option in eclipse, 
and download from https://qmetry.github.io/qaf/editor/bdd/eclipse/

_IntelliJ IDEA users should also install:_
Maven Plugin for IDEA
Cucumber Plugin (Community version only)

Developed by Gavriel Cohen.





